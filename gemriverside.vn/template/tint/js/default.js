(function($) {
  $(document).ready(function() {
    $(".menu-open").click(function() {
      $(".navbar-gem").addClass("active");
      $(".menu-gem-open").hide();
    });

    $('#submit-drive').on('click', function(e) {
        e.preventDefault();
        const name = $('#name').val();
        const number = $('#number').val();
        const content = $('#content').val();
        const url = 'https://script.google.com/macros/s/AKfycbzQcSoX6MrGW9TMp3yVtiFqpji-hSFd55CQE_gNw9fnHYYutYE/exec';
        $.ajax({
            url: url,
            method: "GET",
            crossDomain: true,
            dataType: "json",
            data: { name, number, content},
        }).done(() => {
            
            alert('Cảm ơn bạn đã quan tâm dự án. Tư vấn viên sẽ liên hệ bạn trong vòng 24h.');
            $("input[type=text], textarea").val("");
        });
    });

    $('#submit-drive2').on('click', function(e) {
        const name = $('#name2').val();
        const number = $('#number2').val();
        const choices = $('input[name=choices]').val();
        const url = 'https://script.google.com/macros/s/AKfycbzQcSoX6MrGW9TMp3yVtiFqpji-hSFd55CQE_gNw9fnHYYutYE/exec';
        $.ajax({
            url: url,
            method: "GET",
            crossDomain: true,
            dataType: "json",
            data: { name, number, choices},
        }).done(
            alert('Cảm ơn bạn đã quan tâm dự án. Tư vấn viên sẽ liên hệ bạn trong vòng 24h.')
        );
    });

    // form-liên hệ

    $('#submit-drive-lienhe').on('click', function(e) {
      e.preventDefault();
      const name = $('#name').val();
      const number = $('#number').val();
      const content = $('#content').val();
      const address = $('#address').val();
      const email = $('#email').val();
      const url = 'https://script.google.com/macros/s/AKfycbzQcSoX6MrGW9TMp3yVtiFqpji-hSFd55CQE_gNw9fnHYYutYE/exec';
      $.ajax({
          url: url,
          method: "GET",
          crossDomain: true,
          dataType: "json",
          data: { name, number, content, address, email},
      }).done(() => {
          
          alert('Cảm ơn bạn đã quan tâm dự án. Tư vấn viên sẽ liên hệ bạn trong vòng 24h.');
          $("input[type=text], textarea").val("");
      });
  });

    // end-form-lien hệ



    $(".menu-close").click(function() {
      $(".navbar-gem").removeClass("active");
      $(".menu-gem-open").show();
    });
    setTimeout(function() {
      $("section h2").addClass("active");
      $("#about.detail h5").addClass("active");
      $(".page-title").addClass("active");
    }, 300);
    setTimeout(function() {
      $("#position-introdution .description").addClass("fadeInLeft animated");
      $("#position-introdution .photo").addClass("fadeInRight animated");
    }, 500);
    setTimeout(function() {
      $("#timeline .timeline").addClass("active");
    }, 200);
    setTimeout(function() {
      $("#timeline .timeleft").addClass("fadeInLeft animated");
      $("#timeline .timeright").addClass("fadeInRight animated");
    }, 500);
    setTimeout(function() {
      $("#library .slick-slide .title").addClass("active");
    }, 500);
    setTimeout(function() {
      $("#furniture-detail .grid-item .title").addClass("active");
    }, 500);
    if ($(document).width() < 992) {
      $(".navbar-gem").addClass("mobile");
    }
    $(".btn-dropdown-toggle:not(.fix-title)").each(function() {
      var selectedText = $("li.active a", $(this).next()).html();
      if (selectedText) {
        $(this).html(selectedText);
      }
    });
    $(".btn-dropdown-toggle").click(function() {
      $(this)
        .next()
        .toggleClass("in")
        .slideToggle("fast");
      return false;
    });
    $(document).mouseup(function(e) {
      var container = $("ul.select.in");
      if (
        !container.prev().is(e.target) &&
        !container.is(e.target) &&
        container.has(e.target).length === 0
      ) {
        container.removeClass("in").slideUp("fast");
      }
    });
    $("ul.select").niceScroll({
      cursorcolor: "#113062",
      cursoropacitymin: 0,
      cursoropacitymax: 1,
      cursorwidth: "5px",
      cursorborder: "1px solid #113062",
      cursorborderradius: "0px"
    });
  });
  $(window).resize(function() {
    $(".navbar-gem").removeClass("active");
    $(".menu-gem-open").show();
    if ($(document).width() < 992) {
      $(".navbar-gem").addClass("mobile");
    } else {
      $(".navbar-gem").removeClass("mobile");
    }
  });
  $(".bt-back").click(function() {
    window.history.back();
  });
})(jQuery);
